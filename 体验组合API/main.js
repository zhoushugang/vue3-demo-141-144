// vue2 import Vue from 'vue'
// vue3 createApp 创建vue应用的函数
import { createApp } from 'vue'

import App from './App.vue'

// vue2 new Vue({ render: h=>h(App)  }).$mount('#app')
// 通过createApp和App组件创建一个应用实例，挂载到#app容器
createApp(App).mount('#app')
